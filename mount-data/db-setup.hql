create database if not exists testdb;
use testdb;
create table if not exists employee (
    id INT,
    name STRING,
    dept_id INT
)
row format delimited
fields terminated by ','
lines terminated by '\n'